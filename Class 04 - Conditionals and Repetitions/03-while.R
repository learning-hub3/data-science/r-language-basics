#Laço while

soma <- 0
i <- 10
while(i < 100) {
  soma <- soma+i
  i <- i+5
}
print(soma)

somasParciais <- NULL
soma <- 0
i <- 10
while(i < 100) {
  soma <- soma+i
  somasParciais <- c(somasParciais, soma)
  i <- i+5
}
print(soma)
print(somasParciais)

