#Input by Keyboard
y <- scan() #Reads until find EoF or blank line
print(y)

z <- readLines(n = 2) #Reads 'n' lines
print(z)

getwd() #Gets current directory
setwd("./Class 06 - Files")

data01 <- read.table("./Assets/crabs.csv", header = T, sep = ",", dec = ".")
data01

data02 <- read.csv("./Assets/crabs.csv")
data02

dim(data01)
str(data01)
head(data01)
tail(data01)
names(data01)
summary(data01)