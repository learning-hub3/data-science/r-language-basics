#Transform a specific column

discount90 <- table %>%
  mutate(OldPrice = Price, Discount = 0) %>% #Creates the columns OldPrice and Discount
  mutate(Price = 0.9 * Price) %>% #Changes the prices
  mutate(Discount = OldPrice - Price) #Updates the discount value